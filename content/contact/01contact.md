+++
title = "contact information"
description = ""
tags = []
date = "2021-08-09"
categories = []
menu = "contact"
weight = 1
+++

I currently live in _Fort Collins, Colorado_.

I'm available on Twitter, but often get tired of it:

* **[Twitter](https://twitter.com/mbelotti99)**

My bird watching profiles are only somewhat active. I don't like the competitiveness of lists and lifers. I only submit sightings I want to remember seeing, like an _Asio stygius_ that perched on the same spot at USP for six months in 2019.  

* **[eBird Profile](https://ebird.org/profile/OTU5MDU5/BR_)**

* **[XenoCanto](https://www.xeno-canto.org/contributor/IEQUZLLPND)**

My research pages:

* **[ORCID](https://orcid.org/0000-0001-5595-8775)**

* **[ResearchGate (not updated)](https://www.researchgate.net/profile/Maria_Belotti)**

* **[Lattes (not updated)](http://lattes.cnpq.br/4616346180448502)**

You can email me at: maria dot belotti dot 99 at gmail dot com.
