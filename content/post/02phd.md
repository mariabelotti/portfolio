+++
title = "phd degree studies"
description = ""
tags = []
date = "2024-03-08"
categories = []
menu = "main"
weight = 2
+++

On January 2021, I started my PhD at the Fish, Wildlife and Conservation Department, at Colorado State University. I passed my candidacy exam in 2024 and defended my dissertation on December 4, 2024.

My advisor was Prof. Kyle Horton.

* **Research interests:** weather surveillance radars, ornithology, remote sensing, bird migration, climate change, quantitative ecology

--------------------

## **research outcomes**


* **Peer-reviewed paper**:G. Perez, W. Zhao, Z. Cheng, et al. (2024) Using spatiotemporal information in weather radar data to detect and track communal roosts. _Remote Sensing in Ecology and Conservation_. **[https://doi.org/10.1002/rse2.388](https://doi.org/10.1002/rse2.388)**

* **Peer-reviewed paper**: Belotti, M. C. T. D., Deng, Y., Wenlong, Z. et al. (2023) Long-term analysis of persistence and size of Purple Martin (_Progne subis_) roosts in the US Great Lakes. _Remote Sensing in Ecology and Conservation_. Vol. 9, no. 4, pp. 469–482. **[https://doi.org/10.1002/rse2.323](https://doi.org/10.1002/rse2.323)**

* **Peer-reviewed paper** : Deng, Y., Belotti, M. C. T. D., Wenlong Z. et al. (2022). Quantifying long-term phenological patterns of aerial insectivores roosting in the Great Lakes region using weather surveillance radar. _Global Change Biology_. Vol. 29, no. 5, pp. 1407–1419. **[https://doi.org/10.1111/gcb.16509](https://doi.org/10.1111/gcb.16509)**

* **Peer-reviewed paper**: Haas, E. K., La Sorte, F. A., McCaslin, H. M., Belotti, M. C. T., & Horton, K. G. (2022). The correlation between eBird community science and weather surveillance radar-based estimates of migration phenology. _Global Ecology and Biogeography_, 00, 1– 12. **[https://doi.org/10.1111/geb.13567](https://doi.org/10.1111/geb.13567)**

* **Talk**: Belotti, M. C. T. D., Deng, Y., Wenlong, Z. et al (2022). Long-term analysis of persistence and size of Purple Martin (_Progne subis_) roosts in the US Great Lakes. _PMCA's Purple Martin Conference_. virtual conference.

* **Talk**: Belotti, M. C. T. D., Deng, Y., Wenlong, Z. et al (2022). Long-term analysis of persistence and size of Purple Martin (_Progne subis_) roosts in the US Great Lakes. _AOS & BC 2022 Meeting_. San Juan, Puerto Rico.

* **Poster presentation**: Belotti, C. T. D. M. et al. Long-term analysis of the spatial distribution and habitat associations of Purple Martin (_Progne subis_) roosts in the US Great Lakes. _AOS & SCO-SOC 2021 Meeting_. **[Link to the conference abstracts](https://meeting.americanornithology.org/wp-content/uploads/2021/08/AbstractBook_09August2021.pdf)**.

  * In **[this link](https://gitlab.com/mariabelotti/portfolio/-/blob/24203eb2fe0896f2de01052402f1f93dd1437e6e/static/presentation.pdf)**, you can find the PDF file with the poster.

--------------------

## **lectures**


 * Radar Aeroecology Workshop - Fort Collins, CO, United States - July 31 to August 4, 2022

   * Lecture title: **[Polarimetric Weather Radar Data Products](https://docs.google.com/presentation/d/1iZtNTpOiyH_x9Hm0r09uxwDG0v35eNXp9ZOtGMNdKLA/edit?usp=sharing)**

--------------------

## **graduate level courses taken**


* **Models for Ecological Data**
  * Prof. Tom Hobbs
  * Department Ecosystem Services and Sustainability
  * Colorado State University
  * Fall 2022

* **Bayesian Data Analysis**
  * Prof. Ander Wilson
  * Department of Statistics
  * Colorado State University
  * Spring 2022

* **Radar Meteorology**
  * Prof. Michael Bell
  * Department of Atmospheric Sciences
  * Colorado State University
  * Spring 2022

* **Theory of Population and Evolutionary Ecology**
  * Prof. Dale Lockwood
  * Ecology Department
  * Colorado State University
  * Fall 2021

* **Landscape Ecology Applications**
  * Prof. Kyle Horton
  * Ecology Department
  * Colorado State University
  * Spring 2021

* **Wildlife Population Dynamics**
  * Prof. Dana Winkelman and Prof. David Koons
  * Fish, Wildlife and Conservation Biology Department
  * Colorado State University
  * Spring 2021

--------------------

## **short courses and events**

* **Conference:** American Ornithological Society and Birds Caribbean 2022 Meeting - June 27 to July 1, 2022
  * **Workshops:**
    * _eBird Status: Introduction to eBird Status data products_ - Presented by Matt Strimas-Mackey and Tom Auer
    * _eBird Status: Advanced use of eBird Status data products in R_ - Presented by Matt Strimas-Mackey and Tom Auer

* **Conference:** virtual American Ornithological Society and the Society of Canadian Ornithologists–Société de Ornithologistes du Canada 2021 Meeting - August 9-13, 2021
  * **Workshops**:
    * _Customized BBS trends, trajectories, graphs, and maps using the bbsBayes R package_ - Presented by Brandon Edwards and Adam Smith
    * _Introduction to statistical inference with count data in R_ - Presented by Evan Adams and Beth Ross
