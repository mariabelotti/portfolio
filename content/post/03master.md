+++
title = "master's degree studies"
description = ""
tags = []
date = "2024-03-08"
categories = []
menu = "main"
weight = 3
+++

On November 26th, 2020, I successfully defended my master's thesis at the Mechanical Engineering Department of the Polytechnic School at the University of São Paulo

 * I received a monthly CAPES-PROEX scholarship throughout the two years of my master's program.
 * **Research areas:** non-linear dynamics, collective behaviour, modelling and simulation, self-organization, crowd behaviour.

## **publications and results**

My dissertation is stored at USP's Digital Library of Thesis and Dissertations (**[link](https://www.teses.usp.br/teses/disponiveis/3/3152/tde-19072021-084725/en.php)**). Part of it was also published in the following paper:

* Belotti, M.C.T.D., Martins, F.P.R. (2021) _Analysis and verification of the social forces model in pedestrian lane formation scenarios_. **J Braz. Soc. Mech. Sci. Eng.** 43, 287. https://doi.org/10.1007/s40430-021-03009-1

The data I have used for the analysis, produced with my implementation of the social forces model of crowd behavior, is available on **[Figshare](https://figshare.com/articles/dataset/Results_of_the_social_forces_model_for_crowd_simulation/13210646/1)**.

The code of the implementation is available on **[Gitlab](https://gitlab.com/mariabelotti/social-forces-model)**.

I was a co-author in two publications about COVID-19 epidemiology. In both of them, I contributed with some analysis of the effective reproduction number of COVID-19 in Brazil.

* Buss, L. F. et al (2020). _Three-quarters attack rate of SARS-CoV-2 in the Brazilian Amazon during a largely unmitigated epidemic_. **Science**. https://doi.org/10.1126/science.abe9728

* de Souza, W. M. et al (2020). _Epidemiological and clinical characteristics of the COVID-19 epidemic in Brazil._ **Nature Human Behaviour**, 4(8), 856–865. https://doi.org/10.1038/s41562-020-0928-4

 --------------------

## **service and extension**

* **[g2p2pop Research Coordination Network](https://in.nau.edu/cbi-rcn-g2p2pop)** - Communication Correspondent - August 2019 to present

  * **Activities:**

    - **[Twitter](https://twitter.com/g2p2pop)** account management, curation of content and analytics
    - Static website content development
    - Organization of annual workshops for RCN members


* **[Observatório de Aves do Instituto Butantan](https://www.facebook.com/observatoriodeavesibu/)** - Science Communication - December 2017 to 2019

  * **Activities:**

    - Elaboration and implementation of outreach and citizen science initiatives for the brazilian branch of the purple martin conservation project **[Projeto Andorinha-Azul](http://andorinhaazul.org)**, together with the **[Purple Martin Conservation Association](https://purplemartin.org%20)**, **[INPA](http://portal.inpa.gov.br/)** and **[University of Manitoba](http://www.abclab.ca/)**.
 
--------------------

## **graduate-level courses**

 **Mathematical Models of Social Evolution I**
  * Prof. Eduardo Ottoni, Ph.D and Prof. José Siqueira, Ph.D
  * Psychology Institute of the University of São Paulo
  * March to September 2020

 **Introduction to Data Science**
  * Prof. Pedro Luiz Pizzigatti Corrêa, Ph.D
  * Polytechnic School of the University of São Paulo
  * September to December 2019

 **Introduction to Linear Systems**
  * Prof. Felipe Pait, Ph.D
  * Polytechnic School of the University of São Paulo
  * September to December 2019

 **Mathematical Programming Applied to Transportation Problems**
  * Prof. Marco Antonio Brinati, Ph.D
  * Polytechnic School of the University of São Paulo
  * May to August 2019

 **Urban Science: Technology, Science and Cities**
  * Prof. Fabio Duarte, Ph.D
  * Institute of Mathematics and Statistics of the University of University of São Paulo
  * June 2019

 **Signal Processing**
  * Prof. Flavius Portella Ribas Martins, Ph.D
  * Polytechnic School of the University of São Paulo
  * February to May 2019

 **Statistical Machine Learning**
  * Prof. Fábio Cozman, Ph.D
  * Polytechnic School of the University of São Paulo
  * February to May 2019

 **Introduction to Hierarchical Modeling**
  * Prof. Dr. Gonçalo Nuno Côrte-Real Ferraz de Oliveira
  * Federal University of Rio Grande do Sul
  * April 26 to May 6 2018


--------------------

## **short courses and events**

- **Conference:** International Complex Systems Conference - July 26-31, 2020 - New England Complex Systems Institute

  - I was only able to attend this conference because I was sponsored by the Organizing Committee, to which I'm very thankful.  

- **Conference:** virtual International Statistical Ecology Conference - June 22-26, 2020 - University of Sidney

  - I was only able to attend this conference because I was sponsored by the Organizing Committee, to which I'm very thankful.  

- **Workshop:** Modeling from Genomes to Phenomes to Populations - July 17-19, 2019 - University of Minnesota

  - Organized by the g2p2pop Research Coordination Network
  - My travel expenses were paid by the g2p2pop RCN.
