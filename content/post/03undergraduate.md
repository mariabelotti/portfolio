+++
title = "undergraduate experiences"
description = ""
tags = []
date = "2024-03-08"
categories = []
menu = "main"
weight = 4
+++

In 2018, I received a bachelor's degree in Veterinary Medicine from University Anhembi Morumbi, a private university in São Paulo, Brazil. 

## **publications**

Some of the results of a project I contributed to during my period as an undergraduate student were published in 2021:

* Santos, C.O., Branco, J.M., **Belotti, M.C.T.D. et al**. (2021) Distribution and migration phenology of Purple Martins (Progne subis) in Brazil. **Ornithological Research**. https://doi.org/10.1007/s43388-021-00071-0

--------------------

## **undergraduate research training**

* **[Observatório de Aves do Instituto Butantan](https://www.facebook.com/observatoriodeavesibu/)** - Undergraduate Research - December 2017 to 2019

    - Organization of Instituto Butantan's National Science and Technology Week, a science dissemination event where high school students visit some of the Institute's laboratories.
    - Development of a new citizen science project called **[De Olho na Coruja](https://mariabelotti.gitlab.io/portfolio/science_for_everyone/de_olho_na_coruja.html)**, which was an attempt to mobilize citizens from Bragança Paulista to locate burrowing owl's nests, monitor and help conserve them.
    - Research in owl population monitoring in urban areas. 

* **[Acoustics and Environment Laboratory](http://www.lacmam.poli.usp.br/)** - Polytechnic School (USP) - Undergraduate Research - July to December 2018

  * **Activities:**

   - Construction of a large database of owl vocalizations from citizen science platforms such as XenoCanto and Wikiaves.
   - Assembly and tests of autonomous sound recorders developed in the laboratory.

* **[Evolutive Helminthology Laboratory](http://www.ib.usp.br/~fernando/%20)** - Institute of Biosciences (USP) - Undergraduate Research - February to August 2015

  * **Activities:**

   - Triage an organization of a collection of biological material collected in the field.
   - Using molecular biology techniques for Systematics of parasitic helminths of freshwater neotropical stingrays (Potamotrygonidae).

* **[Núcleo de Pesquisa e Conservação de Cervídeos](https://nupecce.wixsite.com/nupecce%20)** (NUPECCE) da Faculdade de Ciências Agrárias e Veterinária da UNESP - Technical Training Program - July 2013

  * **Activities:**

   - The participants of this program received technical training in several areas of expertise of this research group, including, for example: cytogenetics, molecular phylogenetics and biogeography of brazilian Cervidae and Cervidae physiology and reproduction.


--------------------

## **graduate-level courses**

 **Basic Principles in Phylogenetic Inference**
  * Prof. Fernando Portella de Luna Marques, Ph.D
  * Institute of Biosciences of University of São Paulo
  * February to June 2015

--------------------

## **short courses, workshops, conferences:**

- **Short course**: Birds as Bioindicators - July 23 to 27, 2018 - Butantan Institute

- **Short course**: Collecting, mounting and identifying Insects - April 04 to April 06, 2018 - Butantan Institute

- **Workshop**: Organism-environment interactions: timing, plasticity and metabolic adjustments - February 19 to 21, 2018 - Institute of Biosciences of the University of São Paulo

- **Short course**: Fauna surveys for Environmental Licensing - October 3 to 5, 2017 - CETESB - Companhia Ambiental do São Paulo.

- **Short course**: Techniques used for surveys, censuses and monitoring of birds - July 29 and 30, 2017 - Aiuká - Environmental Solutions.

- **Event**: Meeting of the International Society for History, Philosophy and Social Studies of Biology - July 16 to 21, 2017 - Institute of Biosciences of the University of São Paulo.

- **Short course**: Meeting Temple Grandin - July 04 to 06, 2017 - Meeting and workshop organized by the Department for Preventive Veterinary Medicine and Animal Health of the School of Veterinary Medicine and Animal Science of the University of São Paulo

- **MOOC**: MITx: Introduction to Biology – The Secret of Life - lectures by Eric S. Lander, Ph.D.

- **Short course**: Estimates, Dynamics and Management of Populations of Dogs and Cats – July 01 to 05, 2013 - Instituto Técnico de Educação e Controle Animal (ITEC).

- **Event**: XX Brazilian Congress on Animal Reproduction – June 5 to 7, 2013.

- **Short course**: Biology and Veterinary Medicine of Native Brazilian Birds – Associação Mata Ciliar
– 29 a 31 de março 2013.

- **Short course**: Shelter Veterinary Medicine, SACAVET 2013.

- **Short course**: Animal Behaviour, SACAVET 2013.

- **Short course**: Wildlife Veterinary Medicine, SACAVET 2013.

--------------------

## **volunteering**

- I was a volunteer at SAVE's Program for Wader Monitoring. My job was to monthly count wader species present at a park near São Paulo's Guarapiranga dam.
- I was a member of the administrative support team of the Congress of the Brazilian Wildlife Veterinary Medicine Association in october 2013.
