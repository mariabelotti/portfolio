+++
title = "teaching and mentoring"
description = ""
tags = []
date = "2020-09-07"
categories = []
menu = "main"
weight = 5
+++

I'm particularly passionate about teaching and mentoring. It brings me joy to teach things I struggled so much to learn. 

**Instructor:** FW562: Fish and Wildlife Population Dynamics 
 * Department of Fish, Wildlife, and Conservation Biology
 * Colorado State University
 * Spring 2024

**Teaching assistant** ESS575: Models for Ecological Data
 * Department of Ecosystem Science and Sustainability
 * Colorado State University
 * Instructor: Prof. Tom Hobbs, Ph.D.

**Teaching assistant** ECOL620: Landscape Ecology Applications 
 * Department of Biology
 * Colorado State University
 * Instructor: Prof. Kyle Horton, Ph.D.

**Mentoring** Tido Ramos - Undergraduate student at Colorado State University
 * Project Title: "Comparison of the ability of eBird and Weather Radars to detect swallow and
martin roosts."
 * Work presented as a poster at Colorado State University’s CURC symposium in May
2023.