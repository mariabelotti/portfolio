+++
title = "purple martin project"
description = ""
tags = []
date = "2019-07-23"
categories = []
menu = "scicomm"

+++

_This project was developed in portuguese. Scroll down for English translation._

---------------------

O Projeto Andorinha-Azul é a vertente brasileira de uma rede internacional de pesquisadores com um objetivo em comum: compreender as diversas etapas da migração anual das andorinhas-azuis entre a América do Norte e a América do Sul. Para isso, coletamos dados de ciência cidadã do Wikiaves e do eBird e utilizamos novas tecnologias de rastreamento de aves para compôr um panorama completo do deslocamento das andorinhas ao longo do ano.

Durante a estadia delas no Brasil em 2018-2019, a equipe envolvida no projeto construiu o site (**[que está disponível aqui]([http://andorinhaazul.org/)**) e elaborou material para divulgação e identificação das andorinhas brasileiras. Além disso, somos moderadores do grupo no WhatsApp onde todos são convidados a compartilhar suas fotos, histórias e memórias sobre a passagem das andorinhas pelas suas vidas.

Um dos problemas mais marcantes relacionados ao estudo desta espécie no Brasil é a dificuldade de diferenciação entre ela e as demais andorinhas que frequentam nosso território. Para tentar estabelecer critérios claros de separação, fizemos uma extensa revisão da literatura disponível e montamos dois modelos de guias de identificação para uso principalmente na região da Amazônia (**[neste link](https://gitlab.com/mariabelotti/science_for_anyone/raw/master/purple_martins/chave_de_identifica%C3%A7%C3%A3o.pdf?inline=false)**, você pode obter a chave de identificação, e **[aqui](https://gitlab.com/mariabelotti/science_for_anyone/raw/master/purple_martins/fichas_t%C3%A9cnicas.pdf?inline=false)**, você encontra um conjunto de cards com características de cada espécie).

Em março de 2019, fui para Manaus observar as aves em um imenso dormitório que se formou no Rio Negro entre março e maio de 2019. Mais de 200 mil andorinhas de diversas espécies chegavam todos os dias no mesmo horário e pousavam ao mesmo tempo, na mesma ilha fluvial, em um período de cerca de dez minutos. É um dos fenômenos naturais mais impressionantes que já tive o privilégio de observar, e traz à tona perguntas que mudaram a minha carreira científica.

--------------------

The Purple Martin project is an ongoing collaborative network between several different institutions to understand this species' biology across all of its migration range. During the 2018-2019 migration season in Brazil, we gathered citizen sicence data from many different platforms to create a distribution map of purple martins and to find roosting places in Brazil. Besides using Wikiaves and eBird, we also created a WhatsApp network were anyone was invited to join in and share their sightings, and we contributed as moderators of the discussion.

One interesting challenge we faced with this project is the difficulty in identifying purple martins, since they are very similar to other martins and swallows in Brazil. In order to establish criteria to be used by all birdwatchers, we created two types of guides - an **[identification key](https://gitlab.com/mariabelotti/science_for_anyone/raw/master/purple_martins/chave_de_identifica%C3%A7%C3%A3o.pdf?inline=false)**, and **[a collection of flashcards](https://gitlab.com/mariabelotti/science_for_anyone/raw/master/purple_martins/fichas_t%C3%A9cnicas.pdf?inline=false)** - focusing on species that occur in the Amazon basin.

In march 2019, our Amazon team, with the help of this incipient communication network, found a huge roost of martins and swallows in Rio Negro, and I had the opportunity to see it. More than 200 thousand birds flew towards a relatively small island in the middle of the river, always at the same time of day, for about two months. At a certain point, they form groups that dive in to perch at the same time, in a fascinating coordinated flight. After the first group dives in, it takes about 15 minutes for all of the 200 thousand birds to dive and perch as well. Seeing this was an amazing experience that forever changed my scientific career.
