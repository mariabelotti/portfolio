+++
title = "de olho na coruja"
description = ""
tags = []
date = "2019-07-23"
categories = []
menu = "scicomm"
+++

This project was developed in portuguese. Scroll down for English translation.

---------------------

![Logo do Projeto](/de_olho_na_coruja.png)

Durante o período em que trabalhei no Instituto Butantan, ajudei a construir, junto com o Coletivo Socioambiental de Bragança Paulista e a Associação Bragança Mais, o projeto De Olho na Coruja.

A coruja-buraqueira (//Athene cunicularia//) é a ave símbolo de Bragança Paulista, eleita por voto popular. O projeto De Olho na Coruja tinha como meta criar um mapa coletivo de todas as corujas-buraqueiras de Bragança. Fizemos ações de divulgação de nossos meios de comunicação para os moradores, e os incentivamos a andar pela cidade e nos comunicar se encontrassem alguma coruja.

Ao final de cerca de dois meses de atividades, tínhamos 26 registros de corujas. A população foi muito receptiva a este tipo de iniciativa, se envolvendo tanto nas redes sociais do projeto como nas atividades promovidas em escolas e no Lago do Taboão. Os resultados e outros materiais de divulgação, eram disponibilizados no blog do projeto **[link](https://deolhonascorujas.blogspot.com/**).

O principal objetivo do projeto era promover a cidadania e motivar os moradores de Bragança Paulista a olharem para os espaços urbanos e rurais de forma diferente. Acreditamos que pela criação de vínculos entre os cidadãos e as corujas, podemos fortalecer a comunidade local e incentivar o protagonismo público na vida da cidade.

**[Neste link](https://www.facebook.com/deolhonacoruja/)**, você pode acompanhar as atualizações do projeto pelo Facebook.

**[Aqui](https://deolhonascorujas.blogspot.com/p/ninhos-registrados.html)**, você pode ver o mapa de registros obtidos entre agosto e dezembro de 2018.

Ao final de 2018, no VemPassarinhar, fiz uma apresentação sobre o projeto, contando alguns dos desafios que enfrentamos e as reflexões que surgiram ao longo do piloto. Você pode encontrar **[aqui](https://gitlab.com/mariabelotti/science_for_anyone/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/vempassarinhar2018.pdf)** os slides que elaborei para este evento.

Infelizmente, este projeto foi descontinuado.

--------------------

While working at the Butantan Institute, I helped to create, together with two non-profit organizations (Coletivo Socioambiental e Associação Bragança Mais) from Bragança Paulista, the project "De Olho na Coruja".

Burrowing Owls are the elected symbol of Bragança Paulista. This project's goal was to create a crowdsourced map of all Burrowing Owls' nests in the city. To do that, we encouraged citizens to write us with information about the owls they observed. Our main communication efforts were on Facebook, since we realized that this was the main platform used by our target audience to communicate, but we also had a telephone number and a blog, where we made the results available **[see link here](https://deolhonascorujas.blogspot.com/)**).

Our vision for this project was that in encouraging people to observe their environment looking for owls and monitoring their nests, we would also be promoting public engagement and science education. At the end of our pilot, that lasted two months, we accumulated 26 records of owls. It was a very rich opportunity for me to think about how to engage people from different socio-economical and educational backgrounds in a citizen science project, though some of the difficulties we faced I still have no solution for.
