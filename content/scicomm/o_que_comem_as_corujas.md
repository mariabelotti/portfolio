+++
title = "o que comem as corujas?"
description = ""
tags = []
date = "2020-09-24"
categories = []
menu = "scicomm"

+++
This project was developed in portuguese. Scroll down for English translation.

---------------------

![Science](/IMG_1763_crop_small.JPG)

Durante o período em que trabalhei no Instituto Butantan, traduzi para o cenário brasileiro uma atividade que é muito comum na Europa e nos Estados Unidos: a dissecção de egagrópilas, ou pelotas, de corujas.

Realizamos a atividade duas vezes no espaço da Casa do Horto do Instituto Butantan. Para mim, foram experiências muito transformadoras. As crianças se paramentavam com luvas e máscaras, o que contribuía para a sensação imersiva da experiência, e manipulavam com curiosidade os amontoados de pêlos, ossos, insetos e terra, tentando identificar os fragmentos que encontravam com a ajuda dos pesquisadores do Observatório de Aves.

**[Neste link](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/folha_de_atividade.pdf)**, você encontra a folha de atividade, a ser preenchida pelos participantes com suas anotações sobre o experimento.

**[Aqui](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/o_que_comem_as_corujas__roteiro.pdf)**, adicionei o roteiro da atividade, com sugestões de materiais necessários, percurso da exposição do conteúdo e um breve referencial teórico sobre as corujas-buraqueiras (_Athene cunicularia_), muito comuns no Brasil.

Esse projeto foi apresentado na **[Reunião Científica Anual do Instituto Butantan](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/poster_rca.pdf)**, em 2018. Em 2020, com mais detalhes, será tema de uma conversa no Encontro de Ensino de Ciências por Investigação (em breve adicionarei slides e artigo).

---------------------

During the time I worked at the Butantan Institute, I translated to the brazilian scenario an activity that is very common in Europe and in the United States: the owl pellet dissection.

We held it twice at a very special historical place in the middle of the urban park of the Institute. I was very transformed by these experiences. The children would dress up with gloves and masks, which contributed to the immersive feeling of being a scientist, and then they would curiously manipulate the pellets, trying to identify the fragments of bones and insects, with the help of researchers from the Institute's Bird Observatory.

**[On this link](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/folha_de_atividade.pdf)**, you can find the activity work sheet (in portuguese).

**[Here](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/o_que_comem_as_corujas__roteiro.pdf)**, I've added a portuguese script of the activity, with detailed information about the required materials, some information about Burrowing Owls and the general framework for the theorical exposition.

This project was presented as a poster at the **[Reunião Científica Anual do Instituto Butantan](https://gitlab.com/mariabelotti/science_for_anyone/-/blob/48399908744160821c47b5402a3d3983c304c1a0/owls/what_do_owls_eat%3F/poster_rca.pdf)**, in 2018 (in english).
