+++
title = "okefenokee"
description = ""
tags = []
date = "2024-06-09"
categories = []
menu = "side"
weight = 3
+++

In July of 2023, I developed a machine learning model to predict migration intensity across the state of Georgia (United States) in Spring and Fall. I used data collected by Prof. Kyle Horton from the local weather radars to estimate migration intensity. This data was then paired with environmental variables an used as input to a model that learns environment-intensity associations. This allows us to extrapolate migration intensity from the radar ranges to the entire state. 

The workflow and code for this project is available **[here](https://gitlab.com/mariabelotti/prjct_georgia_on_my_mind)**. 

The output of this project was used to build the two maps shown below. These maps were presented in a letter as part of the public discussion about the permits for implementation of a heavy metal mine by Twin Pines Minerals, LLC in a region close to the Okefenokee National Wildlife Refugee (the permits can be reviewed **[here](https://epd.georgia.gov/draft-permits-public-notice-twin-pines)**).

![](/okefenokee/fall_twinpines_hotspots.png)

![](/okefenokee/spring_twinpines_hotspots.png)