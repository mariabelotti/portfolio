+++
title = "R - short course"
description = ""
tags = []
date = "2019-07-23"
categories = []
menu = "side"
weight = 2
+++

This project was developed in portuguese. It is a short course in R for biologists.

----------------------------

Em 2018, escrevi **[um tutorial introdutório sobre o R](https://gitlab.com/mariabelotti/r-introduction/raw/master/M%C3%A3o%20na%20massa%20-%20Uso%20do%20R.pdf?inline=false)** para alunos interessados em utilizar esta linguagem como uma ferramenta para análise de dados. O objetivo foi passar rapidamente pela mecânica básica do RStudio e ensinar alguns conceito que facilitem a compreensão do resto do material disponível online.

O tutorial é dividido nos seguintes tópicos:

1. Alguns comentários antes de começarmos
2. Como instalar e usar o RStudio
3. Como usar o R para fazer contas
4. Onde procurar ajuda para resolver problemas
5. Variáveis
6. Fazendo um gráfico simples

Para facilitar a aprendizagem e adicionei **[soluções comentadas](https://gitlab.com/mariabelotti/r-introduction/raw/master/script_exemplo.R?inline=false)** (um script em R) para os problemas propostos no tutorial.
