+++
title = "wikiaves scraper"
description = ""
tags = []
date = "2019-07-23"
categories = []
menu = "side"
weight = 3
+++

**About:**

The code **[shared in this repository](https://gitlab.com/mariabelotti/wikiaves_scraper)** is an initial version of a long-term personal project to build a pre-packaged webscraper that facilitates access to Wikiaves's dataset for research purposes.

It is still very raw. I started working on it before the 2018 reshape of Wikiave's website, so a large part of my code is deprecated, but part of it still works and has some documentation.

**Disclaimer:**

With these scripts and little knowledge of Python3.6, you will be able to download a lot of data from Wikiaves. This is not something to be done lightly.

* ALL THE IMAGES AND SOUNDS IN WIKIAVES ARE PROPERTY OF THE AUTHORS THAT POSTED THEM ONLINE. Even though you can't download large resolution files with these scripts, the images downloaded are not yours to share.

* EVERY TIME YOU RUN THE SCRIPT, YOU ARE BURDENING THE WIKIAVES SERVER, AND THIS COSTS MONEY. Right now, the code is organized in a way that makes it harder for you to tweak it into downloading the whole Wikiaves database, but even if you can change it to do so, don't, unless you explicitly ask Reinaldo Guedes.

* IF YOU USE THIS SCRIPT TO DOWNLOAD DATA, HAVE THE DECENCY OF NOT CALLING THE DATA YOURS. No one should have ownership of Wikiaves data, since it is the result of one of the most impressive collective data gathering initiatives in the world.
